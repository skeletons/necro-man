#! /usr/bin/env python3

# Source: https://github.com/gpoore/minted/issues/176#issuecomment-1041378800

import argparse
import sys
import pygments.cmdline as _cmdline
from pygments.lexer import RegexLexer
from pygments.token import *


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', dest='lexer', type=str)
    opts, rest = parser.parse_known_args(args[1:])
    if opts.lexer == 'skel':
        args = [__file__, '-l', __file__ + ':SkelLexer', '-x', *rest]
    _cmdline.main(args)


class SkelLexer(RegexLexer):
    name = 'Skeleton'
    aliases = ['skeleton','skel','sk','necro']
    filenames = ['*.sk']

    tokens = {
        'root': [
            (r'\btype\b', Keyword.Type),
            (r'\bval\b', Keyword.Type),
            (r'\bbinder\b', Keyword.Type),
            (r'\bbranch\b', Keyword.Reserved),
            (r'\bmatch\b', Keyword.Reserved),
            (r'\bstruct\b', Keyword.Reserved),
            (r'\bmodule\b', Keyword.Reserved),
            (r'\bclone\b', Keyword.Reserved),
            (r'\bwith\b', Keyword.Reserved),
            (r'\bor\b', Keyword.Reserved),
            (r'\bend\b', Keyword.Reserved),
            (r'\blet\b', Keyword.Declaration),
            (r'\bin\b', Keyword.Declaration),
            (r'\bopen [a-zA-Z_][_A-Za-z0-9\']*\b', Comment.Preproc),
            (r'\brequire [a-zA-Z_][_A-Za-z0-9\']*\b', Comment.Preproc),
            (r'=', Operator),
            (r'<', Operator),
            (r'>', Operator),
            (r'\|', Operator),
            (r',', Operator),
            (r'\*', Operator),
            (r':', Operator),
            (r';', Operator),
            (r'->', Operator),
            (r'→', Operator),
            (r'λ', Operator),
            (r'\\', Operator),
            (r'%[a-z][_A-Za-z0-9\']*', Name.Decorator),
            (r'[!?&*⊤⊥@$^∀∃+-/~][!?&*⊤⊥@$^∀∃+-/~_A-Za-z0-9\']*', Name.Decorator),
            (r'\(\*', Comment.Multiline, 'comment'),
            (r'\(', Operator),
            (r'\)', Operator),
            (r'[A-Z][_A-Za-z0-9\']*', Name.Class),
            (r'[a-z_][_A-Za-z0-9\']*', Text),
            (r'.', Text),
        ],
        'comment': [
            (r'[^\(*]', Comment.Multiline),
            (r'\(\*', Comment.Multiline, '#push'),
            (r'\*\)', Comment.Multiline, '#pop'),
            (r'[*\(]', Comment.Multiline)
        ]
    }

if __name__ == '__main__':
    main(sys.argv)
