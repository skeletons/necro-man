EMACS=emacs
BATCH_EMACS=$(EMACS) --batch -Q -l init.el

all: manual.pdf manual.html

%.pdf: %.tex pygmentize.py
	latexmk -pdf -shell-escape $*

%.tex: %.org
	$(BATCH_EMACS) $*.org -f org-latex-export-to-latex

%.html: %.org
	$(BATCH_EMACS) $*.org -f org-html-export-to-html

clean:
	rm -rf *.{tex,toc,aux,log,out,fls,fdb_latexmk} _minted*

fullclean: clean
	rm -rf manual.{pdf,html}

.PHONY: all clean fullclean
