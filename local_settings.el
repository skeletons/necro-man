;;;;;;;;;; LaTeX

; Included packages:
(setq org-latex-packages-alist '(("" "minted") ("" "todonotes")))

; Handle source code in LaTeX
(setq org-latex-listings 'minted)


;;;;;;;;;; HTML

; Handle source code in HTML
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(setq package-load-list '((htmlize t)))
(package-initialize)
(unless (package-installed-p 'htmlize)
  (package-refresh-contents)
  (package-install 'htmlize))

(provide 'local_settings)
