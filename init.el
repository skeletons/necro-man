(add-to-list 'load-path (file-name-directory load-file-name))

(require 'local_settings)

(require 'org)
(require 'ox-latex)
